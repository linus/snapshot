Installation and Setup
======================

Requirements
------------

Before actually running snapshot, you need a snapshot pool and a snapshot
postgresql database.

Please follow the section `#Basic setup` from the toplevel README.

Getting snapshot
----------------

Clone the git repository::

    git clone https://salsa.debian.org/snapshot-team/snapshot.git

Run snapshot frontend
---------------------

To run snapshot frontend, please follow the instruction from `docs/deploy.md
<docs/deploy.md>`_.
