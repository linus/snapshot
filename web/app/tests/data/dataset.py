# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Snapshot testing dataset. The dataset has the following structure:
# - timestamp:
#   - archive:
#     - distribution:
#       - package: version
#
# There are currently 3 runs with 3 packages.
# - 'zsh' remains the same
# - 'tmux' gets updated every time
# - 'htop' gets removed

SNAPSHOT_DATASET = {
    '2020-07-11T18:54:38Z': {
        'debian': {
            'unstable': {
                'htop': '1.42',
            },
        },
    },
    '2020-08-11T18:54:38Z': {
        'debian-ports': {
            'unstable': {
                'zsh': '1.10',
            },
        },
        'debian': {
            'unstable': {
                'htop': '1.42',
                'tmux': '1.1',
                'zsh': '1.10',
            },
            'stable': {
                'htop': '1.42',
                'tmux': '1.0',
                'zsh': '1.10',
            },
        },
    },
    '2020-08-12T18:54:38Z': {
        'debian': {
            'unstable': {
                'tmux': '1.2',
                'zsh': '1.10',
            },
            'stable': {
                'htop': '1.42',
                'tmux': '1.1',
                'zsh': '1.10',
            },
        },
    },
    '2020-08-13T18:54:38Z': {
        'debian': {
            'unstable': {
                'tmux': '1.3',
                'zsh': '1.10',
                'libstdc++6': '10.2.1',
            },
            'stable': {
                'tmux': '1.2',
                'zsh': '1.10',
            },
        },
    },
}
