# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2021, 2023 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from json import loads
from pytest import mark


def test_mr_index(client, snapshot_db):
    response = client.get('/mr/package/')
    data = loads(response.data.decode())
    packages = snapshot_db.get_source_packages()

    assert response.status_code == 200

    expected = {
        '_comment': 'foo',
        'result': [
            {'package': package} for package in packages
        ],
    }

    assert data == expected


@mark.parametrize('archive,after,before', (
    (False, False, False),
    (False, True, False),
    (False, False, True),
    (False, True, True),
    (True, True, True),
))
def test_mr_timestamp(client, archive, after, before, dataset, date):
    expected = dataset.get_runs()
    after_time = '2020-08-11T00:00:00Z'
    before_time = '2020-08-12T00:00:00Z'
    archive_name = 'debian-ports'
    query_string = {}

    if after:
        query_string['after'] = after_time

    if before:
        query_string['before'] = before_time

    if archive:
        query_string['archive'] = archive_name

    for key in list(expected.keys()):
        if archive and archive_name != key:
            expected.pop(key)
            continue

        expected[key].sort()
        expected[key] = list(map(lambda x: x.replace('-', '').replace(':', ''),
                                 expected[key]))

        for run in list(expected[key]):
            if after and date(run) < date(after_time):
                expected[key].remove(run)
                continue

            if before and date(run) > date(before_time):
                expected[key].remove(run)
                continue

    response = client.get('/mr/timestamp/', query_string=query_string)
    data = loads(response.data.decode())

    assert data == {
        '_comment': 'foo',
        'result': expected
    }


@mark.parametrize('package,status', (
    ('zsh', 200,),
    ('this-package-does-not-exists', 404,),
))
def test_mr_package(client, package, status, snapshot_db):
    response = client.get(f'/mr/package/{package}/')

    assert response.status_code == status

    if status == 200:
        data = loads(response.data.decode())
        versions = snapshot_db.get_source_versions(package)

        expected = {
            '_comment': 'foo',
            'package': package,
            'result': [
                {'version': version} for version in versions
            ],
        }

        assert data == expected

    # binary route
    response = client.get(f'/mr/binary/{package}/')

    assert response.status_code == status

    if status == 200:
        data = loads(response.data.decode())
        versions = snapshot_db.get_binary_versions(package)

        expected = {
            '_comment': 'foo',
            'binary': package,
            'result': versions,
        }

        assert data == expected


@mark.parametrize('package,version,fileinfo,status', (
    ('zsh', '1.10', '', 200,),
    ('zsh', '1.10', '?fileinfo=1', 200,),
    ('zsh', 'this-version-does-not-exists', '', 404,),
    ('this-package-does-not-exists', '?fileinfo=1', '1.0', 404,),
))
def test_mr_package_version(client, package, version, fileinfo, status,
                            snapshot_db):
    # srcfiles route
    response = client.get(f'/mr/package/{package}/{version}'
                          f'/srcfiles{fileinfo}')

    assert response.status_code == status

    if status == 200:
        data = loads(response.data.decode())
        source_files = snapshot_db.get_source_files(package, version)
        expected = {
            '_comment': 'foo',
            'package': package,
            'version': version,
            'result': [
                {'hash': source_file} for source_file in source_files
            ]
        }

        if fileinfo:
            expected['fileinfo'] = {
                source_file:
                snapshot_db.get_file_info(source_file)
                for source_file in
                source_files
            }

        assert data == expected

    # binfiles route
    response = client.get(f'/mr/binary/{package}/{version}'
                          f'/binfiles{fileinfo}')

    assert response.status_code == status

    if status == 200:
        data = loads(response.data.decode())
        binary_files = snapshot_db.get_binary_files(package, version)
        expected = {
            '_comment': 'foo',
            'binary': package,
            'binary_version': version,
            'result': binary_files,
        }

        if fileinfo:
            expected['fileinfo'] = {
                binary_file['hash']:
                snapshot_db.get_file_info(binary_file['hash'])
                for binary_file in
                binary_files
            }

        assert data == expected

    # binpackages route
    response = client.get(f'/mr/package/{package}/{version}'
                          f'/binpackages')

    assert response.status_code == status

    if status == 200:
        data = loads(response.data.decode())
        binary_packages = snapshot_db.get_binary_packages(package, version)
        expected = {
            '_comment': 'foo',
            'package': package,
            'version': version,
            'result': binary_packages
        }

        assert data == expected

    # allfiles route
    response = client.get(f'/mr/package/{package}/{version}'
                          f'/allfiles{fileinfo}')

    assert response.status_code == status

    if status == 200:
        data = loads(response.data.decode())
        source_files = snapshot_db.get_source_files(package, version)
        files = source_files
        binary_packages = snapshot_db.get_binary_packages(package, version)
        expected = {
            '_comment': 'foo',
            'package': package,
            'version': version,
            'result': {
                'source': [
                    {'hash': source_file} for source_file in source_files
                ],
                'binaries': [],
            }
        }

        for binary_package in binary_packages:
            package = binary_package['name']
            version = binary_package['version']
            binary_files = snapshot_db.get_binary_files(package, version)
            files += [binary_file['hash'] for binary_file in binary_files]
            expected['result']['binaries'].append({
                'files': binary_files,
                'name': package,
                'version': version,
            })

        if fileinfo:
            expected['fileinfo'] = {
                digest:
                snapshot_db.get_file_info(digest)
                for digest in
                files
            }

        assert data == expected

    # binfiles subroute
    response = client.get(f'/mr/package/{package}/{version}'
                          f'/binfiles/{package}/{version}{fileinfo}')

    assert response.status_code == status

    if status == 200:
        data = loads(response.data.decode())
        binary_files = snapshot_db.get_binary_files(package, version)
        expected = {
            '_comment': 'foo',
            'package': package,
            'version': version,
            'binary': package,
            'binary_version': version,
            'result': binary_files,
        }

        if fileinfo:
            expected['fileinfo'] = {
                binary_file['hash']:
                snapshot_db.get_file_info(binary_file['hash'])
                for binary_file in
                binary_files
            }
        assert data == expected


def test_mr_fileinfo(client, snapshot_db):
    response = client.get('/mr/file/not-a-hash/info')

    assert response.status_code == 404

    digest = snapshot_db.get_source_files('zsh', '1.10')[0]
    fileinfo = snapshot_db.get_file_info(digest)
    response = client.get(f'/mr/file/{digest}/info')
    data = loads(response.data.decode())
    expected = {
        '_comment': 'foo',
        'hash': digest,
        'result': fileinfo,
    }

    assert response.status_code == 200
    assert data == expected
