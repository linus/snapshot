# snapshot.debian.org - web frontend
#
# Copyright (c) 2009, 2010 Peter Palfrader
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
from urllib.parse import quote
from os.path import join

from flask import request, Blueprint, render_template, abort, current_app

from snapshot.lib.control_helpers import get_domain, build_url_archive
from snapshot.lib.cache import cache
from snapshot.models.snapshot import get_snapshot_model

log = logging.getLogger(__name__)
router = Blueprint("removal", __name__, url_prefix="/removal")


def _build_crumbs(entry=None):
    crumbs = []

    url = quote(request.environ.get('SCRIPT_NAME')) + "/"
    crumbs.append({'url': url, 'name': get_domain(), 'sep': '|'})

    url += 'removal/'
    crumbs.append({'url': url, 'name': 'removal'})

    if entry is not None:
        url += quote(str(entry))
        crumbs.append({'url': url, 'name': entry, 'sep': ''})

    crumbs[-1]['url'] = None
    return crumbs


@router.route("/")
@cache()
def removal_root():
    removal_root.cache_timeout = current_app.config['CACHE_TIMEOUT_REMOVAL']
    removals = get_snapshot_model().removal_get_list()
    breadcrumbs = _build_crumbs()

    return render_template('removal/removal-list.html', removals=removals,
                           breadcrumbs=breadcrumbs)


@router.route("/<int:key>")
@cache()
def removal_one(key):
    removal_one.cache_timeout = current_app.config['CACHE_TIMEOUT_REMOVAL_ONE']
    snapshot_model = get_snapshot_model()
    removal = snapshot_model.removal_get_one(key)

    if not removal:
        abort(404, 'Removal log not found.')

    files = list(snapshot_model.removal_get_affected(key))

    fileinfo = snapshot_model.packages_get_file_info(files)

    for digest in fileinfo:
        for info in fileinfo[digest]:
            info['dirlink'] = build_url_archive(info['archive_name'],
                                                info['run'], info['path'])
            info['link'] = build_url_archive(info['archive_name'], info['run'],
                                             join(info['path'], info['name']),
                                             isadir=False)

    # reproducible file order
    files.sort(key=lambda a: (fileinfo[a][0]['name'], a))

    return render_template(
        'removal/removal-list-one.html',
        removal=removal,
        files=files,
        fileinfo=fileinfo,
        breadcrumbs=_build_crumbs(key),
        title=f'Removal log #{id}',
    )
